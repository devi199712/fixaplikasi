import React from 'react';
import {
    Animated,
    StyleSheet,
    Text
} from 'react-native';
import {
    Container,
} from 'native-base'

import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

class SplashScreen extends React.Component {
    constructor(props) {
        super(props);
        this.animatedImage = new Animated.Value(0.2);
    }

    static navigationOptions = {
        header: null
    };

    performTimeConsumingTask = async() => {
        return new Promise((resolve) =>
            setTimeout(
                () => { resolve('result') },
                1000
            )
        )
    }

    async componentDidMount() {
        Animated.spring(this.animatedImage, {
            toValue: 1,
            friction: 4,
            delay: 100,
            duration: 100,
            useNativeDriver: true,
        },).start();

        const data = await this.performTimeConsumingTask();

        if (data !== null) {
            this.props.navigation.navigate('Login');
        }
    }

    render() {
        const imageStyle = {
            transform: [{ scale: this.animatedImage }]
        };

        return (
            <Container style={{flex: 1,alignItems: 'center',justifyContent: 'center',backgroundColor: '#1E88E5'}}>
                <Container style={styles.wrapper}>
                    <Animated.View style={[styles.ring, imageStyle]}>
                        <Animated.Image
                            source={require('../../component/img/Tracking1.png')}
                            style={[
                                {
                                    resizeMode: "contain",
                                    width: wp('80%'),
                                    width: hp('50%'),
                                }
                            ]}
                        />
                    </Animated.View>
                </Container>
                <Text style={{fontSize: 15,color: 'white',paddingVertical: 24}}>Powered by Deviyanti Hutari</Text>
            </Container>

        );
    }
}

export default SplashScreen;


const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        display: "flex",
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#1E88E5',
        paddingHorizontal: 16,
    },
});
