import React, {Component} from 'react';
import {TouchableOpacity, AsyncStorage, StyleSheet} from 'react-native';
import Loader from '../../component/loader/loader';
import GlobalConfig from '../../component/config/GlobalConfig';
import CustomHeader from '../../component/header/CustomHeader';
import { Stopwatch, Timer } from 'react-native-stopwatch-timer';
import Svg, {
    Circle,
    Rect,
    Text,
    TSpan,
    G
} from 'react-native-svg';
import {
    Button,
    Container, Icon, Fab,
} from 'native-base';
var that;

export default class DetailProduct extends Component {
    constructor(props) {
        super(props);
        this.state = {
            timerStart: false,
            stopwatchStart: false,
            totalDuration: 90000,
            timerReset: false,
            stopwatchReset: false,
            // x: 1.13437,
            // y: 1.99905,
            // x2: 1.224375,
            // y2: 1.98805,
        };
    }

    toggleStopwatch() {
        this.setState({stopwatchStart: !this.state.stopwatchStart, stopwatchReset: false});
    }

    componentDidMount() {
        AsyncStorage.getItem('idProduct').then(idProduct => {
            AsyncStorage.getItem('nameProduct').then(nameProduct => {
                this.setState({
                    idProduct: JSON.parse(idProduct),
                    nameProduct: JSON.parse(nameProduct),
                });
            });
        });
        this._onFocusListener = this.props.navigation.addListener(
            'didFocus',
            payload => {
                this.loadDetail();
            },
        );
    }

    static navigationOptions = {
        header: null,
    };


    loadDetail() {
        this.toggleStopwatch()
        this.setState({
            loading: true,
        });
        var url = GlobalConfig.URL_SERVER + 'getMac';
        var formData = new FormData();
        formData.append('mac', this.state.idProduct);
        fetch(url, {
            headers: {
                'Content-Type': 'multipart/form-data',
            },
            method: 'POST',
            body: formData,
        })
            .then(response => response.json())
            .then(response => {
                if (response.status == 200) {
                    this.setState({
                        loading: false,
                        dataProduct: response.formula,
                        x: response.formula.x,
                        y: response.formula.y,
                        x2: response.real.x,
                        y2: response.real.y,
                    });
                    this.toggleStopwatch()
                    this.formulaError();
                } else {
                    this.setState({
                        loading: false,
                    });
                    this.toggleStopwatch()
                }

            })
            .catch(error => {
                this.setState({
                    loading: false,
                });
                setTimeout(() => alert('Check your connection and try again'), 312);
            });
    }

    formulaError() {
        let xe = parseFloat(this.state.x);
        let ye = parseFloat(this.state.y);
        let xr = parseFloat(this.state.x2);
        let yr = parseFloat(this.state.y2);

        let f1 = (xe - xr) * (xe - xr);
        let f2 = (ye - yr) * (ye - yr);
        let f3 = f1 + f2;
        let f4 = Math.sqrt(parseFloat(f3));
        this.setState({
            valueError: f4.toFixed(4),
        });
    }

    render() {
        that = this;
        const {x, y, x2, y2} = this.state;
        let xV, yV, x2V, y2V
        if(x != undefined){
            xV = (2.87 - x) * 25
            yV = y * 25
            x2V = (2.87 - x2) * 25
            y2V = y2 * 25
        }

        return (
            <Container style={styles.container}>
                <Loader loading={this.state.loading}/>
                <CustomHeader navigation={this.props.navigation} title={this.state.nameProduct} left={true}/>
                <Container style={styles.content}>
                    <Svg height='100%' width='100%' viewBox="-15 10 100 100">
                        <Circle
                            cx={xV}
                            cy={yV}
                            r='1'
                            fill='red'
                        />
                        <Circle
                            cx={x2V}
                            cy={y2V}
                            r='1'
                            fill='blue'
                        />
                        <Rect
                            x={0.3 * 25}
                            y={1.5 * 25}
                            width={0.90 * 25}
                            height={1.52 * 25}
                            stroke={'#0D47A1'}
                            strokeWidth='0.2'
                        />
                        <Rect
                            x={1.6 * 25}
                            y={1.5 * 25}
                            width={0.90 * 25}
                            height={1.52 * 25}
                            stroke={'#0D47A1'}
                            strokeWidth='0.2'
                        />
                        <Rect
                            x={0}
                            y={4.3 * 25}
                            width={1.50 * 25}
                            height={1.23 * 25}
                            stroke={'#0D47A1'}
                            strokeWidth='0.5'
                        />
                        <Rect
                            x={0}
                            y={0}
                            width={2.87 * 25}
                            height={5.53 * 25}
                            stroke={'#0D47A1'}
                            strokeWidth='0.6'
                        />
                        <Text
                            textAnchor="middle"
                            x={1.5 * 25}
                            y={-0.1 * 25}
                            fill="blue"
                            fontSize={5}
                        >
                            Panjang : 2.87 m
                        </Text>
                        <G rotation="90" origin="100, 50">
                            <Text
                                textAnchor="middle"
                                x={4.6 * 25}
                                y={3 * 25}
                                fill="blue"
                                fontSize={5}
                            >
                                Lebar : 5.53 m
                            </Text>
                        </G>
                        <Circle
                            cx={0 * 25}
                            cy={5.85 * 25}
                            r='1'
                            fill='red'
                        />
                        <Text
                            x={0.1 * 25}
                            y={5.9 * 25}
                            fill="red"
                            fontSize={4}
                        >
                            {/*Titik Pengujian ({x.toFixed(2)}, {y.toFixed(2)})*/}
                            Titik Pengujian ({x}, {y})
                        </Text>
                        <Circle
                            cx={0 * 25}
                            cy={6.15 * 25}
                            r='1'
                            fill='blue'
                        />
                        <Text
                            x={0.1 * 25}
                            y={6.2 * 25}
                            fill="blue"
                            fontSize={4}
                        >
                            Titik Real ({x2}, {y2})
                        </Text>
                        <Text
                            x={0.1 * 25}
                            y={6.5 * 25}
                            fill="blue"
                            fontSize={4}
                        >
                            Nilai Error : {this.state.valueError}
                        </Text>
                    </Svg>
                </Container> 
              <Stopwatch options={styles} laps msecs start={this.state.stopwatchStart} reset={this.state.stopwatchReset} getMsecs={(time) => console.log('Time Access Database = ' + time + ' ms')} />
            </Container>
        );
    }
}


const styles = StyleSheet.create({
    chartRow: {
        flex: 1,
        width: '100%',
    },
    content: {
        paddingHorizontal: 50,
        // width: '100%',
        // height: '100%',
        // alignItems: 'center',
        justifyContent: 'center',
    },
    footer: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        backgroundColor: '#0D47A1',
    },
    text2: {
        fontSize: 20,
        color: 'white',
    },
    container: {
        backgroundColor: 'white',
        borderRadius: 5,
        width: '100%',
    },
    text: {
        fontSize: 30,
        color: 'black',
        marginLeft: 7,
    }
});