import React, {Component} from 'react';
import {View, Text, Image, StyleSheet, TouchableOpacity} from 'react-native';
import Loader from '../../component/loader/loader';
import CustomHeader from '../../component/header/CustomHeader';
import {
    Container,
} from 'native-base';

var that;

export default class HomeAdmin extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    static navigationOptions = {
        header: null,
    };

    componentDidMount() {
        this._onFocusListener = this.props.navigation.addListener(
            'didFocus',
            payload => {

            },
        );
    }


    render() {
        that = this;
        return (
            <Container style={styles.container}>
                <Loader loading={this.state.loading}/>
                <CustomHeader navigation={this.props.navigation} title={'Admin'} left={true}/>
                <View style={{flexDirection: 'column'}}>
                    <TouchableOpacity style={styles.card} onPress={() => this.props.navigation.navigate('User')}>
                        <Image style={styles.img} source={require('../../component/img/customers.png')}/>
                        <Text style={styles.text}>USER</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.card} onPress={() => this.props.navigation.navigate('Product')}>
                        <Image style={styles.img} source={require('../../component/img/product.png')}/>
                        <Text style={styles.text}>PRODUCT</Text>
                    </TouchableOpacity>
                </View>
            </Container>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        backgroundColor: '#1E88E5',
        // flex:1
    },
    card: {
        height: '50%',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomWidth: 5,
        borderBottomColor: '#0D47A1',
    },
    img: {
        width: 150,
        height: 150,
    },
    text: {
        marginTop: 10,
        color: 'white',
        fontSize: 20,
    },
});
