import React, {Component} from 'react';
import {View, Text, TouchableOpacity, TextInput, StyleSheet} from 'react-native';
import GlobalConfig from '../../component/config/GlobalConfig';
import Loader from '../../component/loader/loader';
import CustomHeader from '../../component/header/CustomHeader';

class ProductForm extends Component {
    static navigationOptions = {
        header: null,
    };
    state = {
        name: '',
        mac: '',
    };

    addConfirmation() {
        if (this.state.name == '') {
            alert('Input name product');
        } else if (this.state.mac == '') {
            alert('Input MAC product');
        } else {
            this.add();
        }
    }

    add() {
        this.setState({
            loading: true,
        });
        var url = GlobalConfig.URL_SERVER + 'insertProduct';
        var formData = new FormData();
        formData.append('name', this.state.name);
        formData.append('mac', this.state.mac);
        fetch(url, {
            headers: {
                'Content-Type': 'multipart/form-data',
            },
            method: 'POST',
            body: formData,
        })
            .then(response => response.json())
            .then(response => {
                if (response.status == 200) {
                    this.setState({
                        loading: false,
                    });
                    alert('Product added successfully');
                    this.props.navigation.navigate('Product');
                } else {
                    this.setState({
                        loading: false,
                    });
                    alert('Save product failed');
                }

            })
            .catch(error => {
                this.setState({
                    loading: false,
                });
                setTimeout(() => alert('Check your connection and try again'), 312);
            });
    }

    render() {
        return (
            <View style={styles.container}>
                <Loader loading={this.state.loading}/>
                <CustomHeader navigation={this.props.navigation} title={'Product Form'} left={true}/>
                <TextInput style={[styles.input, {marginTop: 30}]}
                           placeholder="Name Product"
                           placeholderTextColor="white"
                           onChangeText={text => this.setState({name: text})}
                           value={this.props.name}/>

                <TextInput style={styles.input}
                           placeholder="MAC"
                           placeholderTextColor="white"
                           onChangeText={text => this.setState({mac: text})}
                           value={this.props.mac}/>
                <TouchableOpacity
                    style={styles.submitButton}
                    onPress={() => this.addConfirmation()}>
                    <Text style={styles.submitButtonText}> SAVE PRODUCT </Text>
                </TouchableOpacity>
            </View>
        );
    }
}

export default ProductForm;

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#1E88E5',
        flex: 1,
    },
    input: {
        margin: 15,
        backgroundColor: 'rgba(255,255,255,0.3)',
        borderRadius: 25,
        height: 50,
        paddingHorizontal: 16,
        color: 'white',
    },
    submitButton: {
        backgroundColor: '#0D47A1',
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 15,
        height: 50,
        borderRadius: 25,
        marginTop: 50,
    },
    submitButtonText: {
        color: 'white',
        textAlign: 'center',
    },
});
