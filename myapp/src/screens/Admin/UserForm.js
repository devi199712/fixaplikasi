import React, {Component} from 'react';
import {View, Text, TouchableOpacity, TextInput, StyleSheet} from 'react-native';
import GlobalConfig from '../../component/config/GlobalConfig';
import Loader from '../../component/loader/loader';
import CustomHeader from '../../component/header/CustomHeader';

class UserForm extends Component {
    state = {
        nama: '',
        email: '',
        password: '',
    };

    static navigationOptions = {
        header: null,
    };

    registerConfirmation() {
        if (this.state.nama == '') {
            alert('Input user name');
        } else if (this.state.email == '') {
            alert('Input email user');
        } else if (this.state.password == '') {
            alert('Input password');
        } else {
            this.add();
        }
    }

    add() {
        this.setState({
            loading: true,
        });
        var url = GlobalConfig.URL_SERVER + 'insertUser';
        var formData = new FormData();
        formData.append('role_id', 2);
        formData.append('nama', this.state.nama);
        formData.append('email', this.state.email);
        formData.append('password', this.state.password);
        fetch(url, {
            headers: {
                'Content-Type': 'multipart/form-data',
            },
            method: 'POST',
            body: formData,
        })
            .then(response => response.json())
            .then(response => {
                if (response.status == 200) {
                    this.setState({
                        loading: false,
                    });
                    alert('User added successfully');
                    this.props.navigation.navigate('User');
                } else {
                    this.setState({
                        loading: false,
                    });
                    alert('Save user failed');
                }

            })
            .catch(error => {
                this.setState({
                    loading: false,
                });
                setTimeout(() => alert('Check your connection and try again'), 312);
            });
    }

    render() {
        return (
            <View style={styles.container}>
                <Loader loading={this.state.loading}/>
                <CustomHeader navigation={this.props.navigation} title={'User Form'} left={true}/>
                <TextInput style={[styles.input, {marginTop: 30}]}
                           placeholder="Nama"
                           placeholderTextColor="white"
                           onChangeText={text => this.setState({nama: text})}
                           value={this.props.nama}/>

                <TextInput style={styles.input}
                           placeholder="Email"
                           placeholderTextColor="white"
                           onChangeText={text => this.setState({email: text})}
                           value={this.props.email}/>


                <TextInput style={styles.input}
                           placeholder="Password"
                           placeholderTextColor="white"
                           secureTextEntry={true}
                           onChangeText={text => this.setState({password: text})}
                           value={this.props.password}/>

                <TouchableOpacity
                    style={styles.submitButton}
                    onPress={() => this.registerConfirmation()}>
                    <Text style={styles.submitButtonText}> SAVE USER </Text>
                </TouchableOpacity>
            </View>
        );
    }
}

export default UserForm;

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#1E88E5',
        flex: 1,
    },
    input: {
        margin: 15,
        backgroundColor: 'rgba(255,255,255,0.3)',
        borderRadius: 25,
        height: 50,
        paddingHorizontal: 16,
        color: 'white',
    },
    submitButton: {
        backgroundColor: '#0D47A1',
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 15,
        height: 50,
        borderRadius: 25,
        marginTop: 50,
    },
    submitButtonText: {
        color: 'white',
        textAlign: 'center',
    },
});
