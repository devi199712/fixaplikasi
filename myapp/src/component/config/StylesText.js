import {StyleSheet} from 'react-native';
import ColorsSource from './ColorsSource';
import colors from './ColorsSource';
import {RFValue} from 'react-native-responsive-fontsize';
const font1 = 'Roboto-Medium';
const font2 = 'Roboto-Light';
const font3 = 'Roboto-Regular';
const font4 = 'ProductSans-Regular';
const font5 = 'ProductSans-Medium';
const font6 = 'ProductSans-Light';

const StylesText = StyleSheet.create({
    //SIZE FONT _ COLOR _ FONT FAMILY _ FONT WEIGHT
    //BLACK
    f12_cb1_fF2_tAl: {
        fontSize: RFValue(12),
        color: colors.black_1st,
        textAlign: 'left',
        fontFamily: font2,
    },

    f13_cb1_fF2_tAc: {
        fontSize: RFValue(13),
        color: colors.black_1st,
        textAlign: 'center',
        fontFamily: font2,
    },

    f13_cb1_fF5_tAl: {
        fontSize: RFValue(13),
        color: colors.black_1st,
        textAlign: 'left',
        fontFamily: font5,
    },

    f13_cb1_fF4_tAl: {
        fontSize: RFValue(13),
        color: colors.black_1st,
        textAlign: 'left',
        fontFamily: font4,
    },

    f13_cb1_fF6_tAl: {
        fontSize: RFValue(13),
        color: colors.black_1st,
        textAlign: 'left',
        fontFamily: font6,
    },
    f13_cb1_fF6_tAc: {
        fontSize: RFValue(13),
        color: colors.black_1st,
        textAlign: 'center',
        fontFamily: font6,
    },

    f15_cb1_fF1_fW: {
        fontSize: RFValue(15),
        color: ColorsSource.black_1st,
        fontWeight: 'bold',
        fontFamily: font1,
    },

    f15_cb1_fF5: {
        fontSize: RFValue(15),
        color: ColorsSource.black_1st,
        fontFamily: font5,
    },

    f17_cb1_fF5_tAc: {
        fontSize: RFValue(17),
        color: colors.black_1st,
        textAlign: 'center',
        fontFamily: font5,
    },


    f20_cb1_fF1_fW: {
        fontSize: RFValue(20),
        color: ColorsSource.black_1st,
        fontWeight: 'bold',
        fontFamily: font5,
    },



    //WHITE
    f10_cw1_fF5_tAc: {
        fontSize: RFValue(10),
        color: colors.white_1st,
        textAlign: 'center',
        fontFamily: font5,
    },

    f13_cw1_fF2_tAc: {
        fontSize: RFValue(13),
        color: colors.white_1st,
        textAlign: 'center',
        fontFamily: font3,
    },

    f13_cw1_fF5_tAc: {
        fontSize: RFValue(13),
        color: colors.white_1st,
        textAlign: 'center',
        fontFamily: font5,
    },

    f13_cw1_fF2_tAl: {
        fontSize: RFValue(13),
        color: colors.white_1st,
        textAlign: 'left',
        fontFamily: font3,
    },

    f17_cw1_fF1_tAl: {
        fontSize: RFValue(17),
        color: colors.white_1st,
        textAlign: 'left',
        fontFamily: font5,
    },
    f20_cw1_fF1_tAl: {
        fontSize: RFValue(20),
        color: colors.white_1st,
        textAlign: 'left',
        fontFamily: font1,
    },
    f25_cw1_fF1_tAl: {
        fontSize: RFValue(25),
        color: colors.white_1st,
        textAlign: 'left',
        fontFamily: font1,
    },

    //GRAY
    f12_cg2_fF2_tAl: {
        fontSize: RFValue(12),
        color: colors.gray_2st,
        textAlign: 'left',
        fontFamily: font4,
    },

    f13_cg2_fF2_tAl: {
        fontSize: RFValue(13),
        color: colors.gray_2st,
        textAlign: 'left',
        fontFamily: font3,
    },

    f13_cg2_fF2_tAc: {
        fontSize: RFValue(13),
        color: colors.gray_2st,
        textAlign: 'center',
        fontFamily: font3,
    },

    f25_cg2_fF3_tAc: {
        fontSize: RFValue(25),
        color: colors.gray_2st,
        textAlign: 'center',
        fontFamily: font3,
    },

    //PURPLE
    f13_cp1_fF5_fW: {
        fontSize: RFValue(13),
        fontWeight: 'bold',
        color: colors.purple_1st,
        fontFamily: font5,
    },
    f15_cp1_fF1_fW: {
        fontSize: RFValue(15),
        fontWeight: 'bold',
        color: colors.purple_1st,
        fontFamily: font1,
    },
    f15_cp1_fF5_fW: {
        fontSize: RFValue(15),
        fontWeight: 'bold',
        color: colors.purple_1st,
        fontFamily: font5,
    },
    f20_cp1_fF1_fW: {
        fontSize: RFValue(20),
        fontWeight: 'bold',
        color: colors.purple_1st,
        fontFamily: font1,
    },
    f25_cp1_fF1_tAl: {
        fontSize: RFValue(25),
        color: colors.purple_1st,
        textAlign: 'left',
        fontFamily: font1,
    },

    //RED
    f10_cr1_fF1_tAl: {
        fontSize: RFValue(10),
        fontFamily: font1,
        color: colors.red_1st,
        textAlign: 'left',
    },
    f17_cr1_fF5_tAc: {
        fontSize: RFValue(17),
        fontFamily: font5,
        color: colors.red_1st,
        textAlign: 'center',
    },
});

export default StylesText;
