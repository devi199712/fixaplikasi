export default {
  white_1st : '#FFFFFF',
  white_2st : '#F7F9FB',
  white_3st : '#F2F2F2',
  white_4st : '#f4f4f4',
  white_5st : '#dfdfdf',
  white_6st : '#F6F6F6',
  white_7st : '#E6E6E6',
  gray_1st : '#A8A8A8',
  gray_2st : '#808080',
  gray_3st : '#848484',
  blue_1st : '#3443A0',
  blue_2st : "#4179e8",
  blue_3st : "#118EEA",
  blue_4st : "#428DFF",
  blue_5st : "#4b4b7d",
  green_1st : "#8FC541",

  red_1st : '#C70D3A',
  black_1st : '#000000',
  orange_1st : '#FF8C00',
  yellow_1st : '#eeb416',

};