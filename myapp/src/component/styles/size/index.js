import {
    Dimensions,
} from 'react-native';

const size = Dimensions.get('window').width;

export default {
    small_00 : 0.02 * size,
    small_01 : 0.025 * size,
    small_02 : 0.03 * size,
    small_03 : 0.035 * size,
    small_04 : 0.04 * size,
    small_05 : 0.045 * size,
    small_06 : 0.05 * size,
    small_07 : 0.055 * size,
    small_08 : 0.06 * size,
    small_09 : 0.065 * size,
    small_10 : 0.07 * size,


    large_00 : 0.10 * size,
    large_01 : 0.15 * size,
    large_02 : 0.20 * size,
    large_03 : 0.20 * size,
    large_04 : 0.20 * size,
};
