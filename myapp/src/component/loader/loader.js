import React from 'react';
import {
    StyleSheet,
    View,
    Modal,
    ActivityIndicator,
} from 'react-native';
import ColorsSource from '../config/ColorsSource';

const Loader = props => {
    const {
        loading,
        title,
        ...attributes
    } = props;


    this.state = {
        isVisible: false,
    };

    return (
        <Modal
            transparent={true}
            animationType={'none'}
            visible={loading ? loading : this.state.isVisible}
            onRequestClose={() => {
                console.log('close modal');
            }}
        >
            <View style={styles.modalBackground}>
                <ActivityIndicator size="large" color={ColorsSource.white_1st}/>
            </View>
        </Modal>
    );
};

const styles = StyleSheet.create({
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-around',
    },
});
export default Loader;
