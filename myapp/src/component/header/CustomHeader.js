import React, {Component} from 'react';
import {
  Platform,
  Image,
  AsyncStorage,
  TextInput,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {
  Footer,
  FooterTab,
  Text,
  Button,
  Icon,
  View,
  Left,
  Body,
  Title,
  Right,
  Header,
} from 'native-base';
import Entypo from 'react-native-vector-icons/Entypo';
import AntDesign from 'react-native-vector-icons/AntDesign';
class CustomHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {

    return (
        <View>
          <Header style={[styles.header]}>
            <Left style={{flex: 1}}>
            
                <TouchableOpacity
                    transparent
                    onPress={() => this.props.navigation.goBack()}
                    style={styles.btnBack}
                >
                  <Entypo
                      name='chevron-small-left'
                      size={25}
                      color={'white'}
                  />
                </TouchableOpacity>
            
            </Left>
            <Body style={{flex: 3, alignItems: 'center'}}>
              <Text style={{color:'white'}}>{this.props.title}</Text>
            </Body>
            <Right style={{flex: 1}}/>
          </Header>
        </View>
    );
  }
}

export default CustomHeader;

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#0D47A1',
  },
  btnBack: {
    width: 30,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconMenu: {
    width: 20,
    height: 20,
  },
});
