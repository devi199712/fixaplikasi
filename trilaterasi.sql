-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 30, 2020 at 04:13 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `trilaterasi`
--

-- --------------------------------------------------------

--
-- Table structure for table `beacon`
--

CREATE TABLE `beacon` (
  `id` int(11) NOT NULL,
  `mac` varchar(50) NOT NULL,
  `rssi` int(6) NOT NULL,
  `source` varchar(20) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `beacon`
--

INSERT INTO `beacon` (`id`, `mac`, `rssi`, `source`, `time`) VALUES
(228, 'ac:23:3f:26:08:6e', -72, 'raspi1', '2020-06-18 15:41:59'),
(229, 'ac:23:3f:26:08:6f', -67, 'raspi1', '2020-06-18 15:41:59'),
(230, 'ac:23:3f:26:08:6e', -66, 'raspi2', '2020-06-18 15:47:23'),
(232, 'ac:23:3f:26:08:6f', -68, 'raspi2', '2020-06-18 15:47:24'),
(236, 'ac:23:3f:26:08:6e', -83, 'raspi3', '2020-06-18 15:52:04'),
(238, 'ac:23:3f:26:08:6f', -76, 'raspi3', '2020-06-18 15:50:45'),
(239, 'ac:23:3f:26:08:6f', -65, 'raspi3', '2020-06-23 04:51:34'),
(240, 'ac:23:3f:26:08:6e', -64, 'raspi3', '2020-06-23 04:51:34'),
(241, '20:1a:79:51:e0:60', -62, 'raspi3', '2020-06-23 04:51:34'),
(242, '44:3a:a4:36:ea:3e', -82, 'raspi3', '2020-06-23 04:51:34'),
(243, 'ac:23:3f:26:08:6f', -62, 'raspi3', '2020-06-23 04:52:12'),
(244, 'ac:23:3f:26:08:6e', -71, 'raspi3', '2020-06-23 04:52:12'),
(245, '20:1a:79:51:e0:60', -64, 'raspi3', '2020-06-23 04:52:13'),
(246, '44:3a:a4:36:ea:3e', -73, 'raspi3', '2020-06-23 04:52:13'),
(247, 'ac:23:3f:26:08:6f', -65, 'raspi3', '2020-06-23 04:53:28'),
(248, 'ac:23:3f:26:08:6e', -64, 'raspi3', '2020-06-23 04:53:29'),
(249, '42:e2:60:cc:c0:c7', -53, 'raspi3', '2020-06-23 04:53:29'),
(250, '20:1a:79:51:e0:60', -56, 'raspi3', '2020-06-23 04:53:29'),
(251, '44:3a:a4:36:ea:3e', -67, 'raspi3', '2020-06-23 04:53:29');

-- --------------------------------------------------------

--
-- Table structure for table `koordinat`
--

CREATE TABLE `koordinat` (
  `nomor` int(11) NOT NULL,
  `mac` varchar(30) NOT NULL,
  `x` float NOT NULL,
  `y` float NOT NULL,
  `rak` varchar(10) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `koordinat`
--

INSERT INTO `koordinat` (`nomor`, `mac`, `x`, `y`, `rak`, `time`) VALUES
(253, 'ac:23:3f:26:08:6e', 1.50771, -1.09449, 'X', '2020-06-18 15:36:19'),
(254, 'ac:23:3f:26:08:6f', 1.40455, 1.45816, 'X', '2020-06-18 15:36:19'),
(255, 'ac:23:3f:26:08:6e', 1.54043, -1.09449, 'X', '2020-06-18 15:54:15'),
(256, 'ac:23:3f:26:08:6f', 1.419, 1.15502, 'X', '2020-06-18 15:54:15');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `mac` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `mac`) VALUES
(20, 'Connector', 'ac:23:3f:26:08:6e'),
(21, 'Transistor', 'ac:23:3f:26:08:6f');

-- --------------------------------------------------------

--
-- Table structure for table `real`
--

CREATE TABLE `real` (
  `nomor` int(11) NOT NULL,
  `mac` varchar(50) NOT NULL,
  `x` float NOT NULL,
  `y` float NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `real`
--

INSERT INTO `real` (`nomor`, `mac`, `x`, `y`, `timestamp`) VALUES
(1, 'ac:23:3f:26:08:6e', 0.75, 1.8, '2020-06-19 00:36:20'),
(2, 'ac:23:3f:26:08:6f', 2.25, 1.8, '2020-06-19 00:39:11');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `role_id` int(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `nama`, `email`, `password`) VALUES
(7, 1, 'admin', 'admin@gmail.com', 'admin'),
(8, 2, 'Devi', 'Devi@gmail.com', '12345');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `beacon`
--
ALTER TABLE `beacon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `koordinat`
--
ALTER TABLE `koordinat`
  ADD PRIMARY KEY (`nomor`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `real`
--
ALTER TABLE `real`
  ADD PRIMARY KEY (`nomor`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `beacon`
--
ALTER TABLE `beacon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=252;

--
-- AUTO_INCREMENT for table `koordinat`
--
ALTER TABLE `koordinat`
  MODIFY `nomor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=257;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `real`
--
ALTER TABLE `real`
  MODIFY `nomor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
