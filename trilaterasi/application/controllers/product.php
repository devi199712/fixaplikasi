<?php
defined('BASEPATH') or exit('No direct script allowed');

/*----------------------------------------REQUIRE THIS PLUGIN----------------------------------------*/
require APPPATH . '/libraries/REST_Controller.php';
//use Restserver\Libraries\REST_Controller;

class product extends REST_Controller
{
    /*----------------------------------------CONSTRUCTOR----------------------------------------*/
    function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->database();
    }

    /*----------------------------------------GET KONTAK----------------------------------------*/
    function index_get()
    {
        $product = $this->db->get('product')->result();
    
        $obj = array(
            'status' => '200',
            'data'=> $product
        );
        echo json_encode($obj);

    }

    function index_post()
    {
        $data = array(
            'mac'  =>    $this->post('mac'),
            'name'    =>    $this->post('name'),
        );
        $insert = $this->db->insert('product', $data);

        if ($insert) {
             $obj = array(
                'status' => 200,
                'data'=> $data
            );
        
        } else {
            $obj = array(
                'status' => 404,
                'data'=> $data
            );
        }
        echo json_encode($obj);  
    }

    function index_delete()
    {

        $id = $this->delete('id');

        $this->db->where('id', $id);

        $delete = $this->db->delete('product');

        if ($delete) {
            $obj = array(
                'status' => 200,
                'message'=> 'Product Berhasil dihapus'
            );
        } else {
            $obj = array(
                'status' => 404,
                'message'=> 'Product Gagal dihapus'
            );
        }
        echo json_encode($obj);
    }
}