<?php
defined('BASEPATH') or exit('No direct script allowed');

/*----------------------------------------REQUIRE THIS PLUGIN----------------------------------------*/
require APPPATH . '/libraries/REST_Controller.php';
//use Restserver\Libraries\REST_Controller;

class real extends REST_Controller
{
    /*----------------------------------------CONSTRUCTOR----------------------------------------*/
    function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->database();
    }

    /*----------------------------------------GET KONTAK----------------------------------------*/
    function index_get()
    {
        $x = $this->get('nomor');

        if ($x == '') {
            $kontak = $this->db->get('real')->result();
        } else {
            $this->db->where('nomor', $x);
            $kontak = $this->db->get('real')->result();
        }

        $this->response($kontak, 200);
    }

    function index_post()
    {
        $data = array(
            'mac'  =>    $this->post('mac'),
            'x'    =>    $this->post('x'),
            'y'    =>    $this->post('y'),
        );
        $insert = $this->db->insert('real', $data);

        if ($insert) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    function index_put()
    {
        $mac = $this->put('mac');
        $data = array(
            'x'    =>    $this->put('x'),
            'y'    =>    $this->put('y'),
        );

        $this->db->where('mac', $mac);
        
        $update = $this->db->update('real', $data);

        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail'), 502);
        }
    }
    
    function index_delete()
    {
        $x = $this->delete('x');

        $this->db->where('x', $x);

        $delete = $this->db->delete('real');

        if ($delete) {
            $this->response(array('status' => 'success'), 201);
        } else {
            $this->response(array('status' => 'fail'), 502);
        }
    }
}