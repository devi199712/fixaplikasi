<?php
defined('BASEPATH') or exit('No direct script allowed');

/*----------------------------------------REQUIRE THIS PLUGIN----------------------------------------*/
require APPPATH . '/libraries/REST_Controller.php';
//use Restserver\Libraries\REST_Controller;

class removeUsers extends REST_Controller
{
    /*----------------------------------------CONSTRUCTOR----------------------------------------*/
    function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->database();
    }

    /*----------------------------------------GET KONTAK----------------------------------------*/
   
    function index_post()
    {

        $id =    $this->post('id');
        $this->db->where('id', $id);

        $delete = $this->db->delete('users');

        if ($delete) {
            $obj = array(
                'status' => 200,
                'message'=> 'User Berhasil dihapus'
            );
        } else {
            $obj = array(
                'status' => 404,
                'message'=> 'User Gagal dihapus'
            );
        }
        echo json_encode($obj);
    }

}