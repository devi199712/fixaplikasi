<?php
defined('BASEPATH') or exit('No direct script allowed');

/*----------------------------------------REQUIRE THIS PLUGIN----------------------------------------*/
require APPPATH . '/libraries/REST_Controller.php';
//use Restserver\Libraries\REST_Controller;

class usersLogin extends REST_Controller
{
    /*----------------------------------------CONSTRUCTOR----------------------------------------*/
    function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->database();
    }

    /*----------------------------------------GET KONTAK----------------------------------------*/
    function index_post()
    {
        $email = $this->post('email');
        $password = $this->post('password');

        $array = array('email' => $email, 'password' => $password);

        $this->db->where($array);
        $data = $this->db->get('users')->row();

        if ($data) {
             $obj = array(
                'status' => 200,
                'data'=> $data
            );
        
        } else {
            $obj = array(
                'status' => 404,
                'data'=> $data
            );
        }
        echo json_encode($obj);  
    }
}