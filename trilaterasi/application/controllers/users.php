<?php
defined('BASEPATH') or exit('No direct script allowed');

/*----------------------------------------REQUIRE THIS PLUGIN----------------------------------------*/
require APPPATH . '/libraries/REST_Controller.php';
//use Restserver\Libraries\REST_Controller;

class users extends REST_Controller
{
    /*----------------------------------------CONSTRUCTOR----------------------------------------*/
    function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->database();
    }

    /*----------------------------------------GET KONTAK----------------------------------------*/
    function index_get()
    {
        $users = $this->db->get('users')->result();
    
        $obj = array(
            'status' => '200',
            'data'=> $users
        );
        echo json_encode($obj);

    }

    function index_post()
    {
        $data = array(
            'role_id'  =>    $this->post('role_id'),
            'nama'    =>    $this->post('nama'),
            'email'    =>    $this->post('email'),
            'password'    =>    $this->post('password'),
        );
        $insert = $this->db->insert('users', $data);

        if ($insert) {
             $obj = array(
                'status' => 200,
                'data'=> $data
            );
        
        } else {
            $obj = array(
                'status' => 404,
                'data'=> $data
            );
        }
        echo json_encode($obj);  
    }

    function index_delete()
    {
        $id = $this->delete('id');

        $this->db->where('id', $id);

        $delete = $this->db->delete('users');

        if ($delete) {
            $obj = array(
                'status' => 200,
                'message'=> 'User Berhasil dihapus'
            );
        } else {
            $obj = array(
                'status' => 404,
                'message'=> 'User Gagal dihapus'
            );
        }
        echo json_encode($obj);
    }
}