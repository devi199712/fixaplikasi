<?php
defined('BASEPATH') or exit('No direct script allowed');

/*----------------------------------------REQUIRE THIS PLUGIN----------------------------------------*/
require APPPATH . '/libraries/REST_Controller.php';
//use Restserver\Libraries\REST_Controller;

class removeProduct extends REST_Controller
{
    /*----------------------------------------CONSTRUCTOR----------------------------------------*/
    function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->database();
    }

    /*----------------------------------------GET KONTAK----------------------------------------*/
   
    function index_post()
    {

        $id =    $this->post('id');
        $this->db->where('id', $id);

        $delete = $this->db->delete('product');

        if ($delete) {
            $obj = array(
                'status' => 200,
                'message'=> 'Product Berhasil dihapus'
            );
        } else {
            $obj = array(
                'status' => 404,
                'message'=> 'Product Gagal dihapus'
            );
        }
        echo json_encode($obj);
    }

}