<?php
defined('BASEPATH') or exit('No direct script allowed');

/*----------------------------------------REQUIRE THIS PLUGIN----------------------------------------*/
require APPPATH . '/libraries/REST_Controller.php';
//use Restserver\Libraries\REST_Controller;

class koordinat extends REST_Controller
{
    /*----------------------------------------CONSTRUCTOR----------------------------------------*/
    function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->database();
    }

    /*----------------------------------------GET KONTAK----------------------------------------*/
    function index_get()
    {
        $x = $this->get('nomor');

        if ($x == '') {
            $kontak = $this->db->get('koordinat')->result();
        } else {
            $this->db->where('nomor', $x);
            $kontak = $this->db->get('koordinat')->result();
        }

        $this->response($kontak, 200);
    }

    function index_post()
    {
        $x = $this->post('x');
        $y = $this->post('y');
        if (($x >= 1.8 and $x <= 2.7)and($y >= 1.5 and $y <= 3)) {
            $rak = "B";
        }elseif(($x >= 1.2 and $x <= 0.3)and($y >= 1.5 and $y <= 3)){
            $rak = "A";
        }else{
            $rak = "X";
        }
        $data = array(
            'mac'  =>    $this->post('mac'),
            'x'    =>    $x,
            'y'    =>    $y,
            'rak' => $rak
        );
        $insert = $this->db->insert('koordinat', $data);

        if ($insert) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    function index_put()
    {
        $mac = $this->put('mac');
        $x = $this->put('x');
        $y = $this->put('y');
        if (($x >= 1.8 and $x <= 2.7)and($y >= 1.5 and $y <= 3)) {
            $rak = "B";
        }elseif(($x >= 1.2 and $x <= 0.3)and($y >= 1.5 and $y <= 3)){
            $rak = "A";
        }else{
            $rak = "X";
        }
        $data = array(
            'x'    =>    $x,
            'y'    =>    $y,
            'rak' => $rak
        );

        $this->db->where('mac', $mac);
        
        $update = $this->db->update('koordinat', $data);

        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail'), 502);
        }
    }
    
    function index_delete()
    {
        $x = $this->delete('x');

        $this->db->where('x', $x);

        $delete = $this->db->delete('koordinat');

        if ($delete) {
            $this->response(array('status' => 'success'), 201);
        } else {
            $this->response(array('status' => 'fail'), 502);
        }
    }
}