<?php
defined('BASEPATH') or exit('No direct script allowed');

/*----------------------------------------REQUIRE THIS PLUGIN----------------------------------------*/
require APPPATH . '/libraries/REST_Controller.php';
//use Restserver\Libraries\REST_Controller;

class getKoordinat extends REST_Controller
{
    /*----------------------------------------CONSTRUCTOR----------------------------------------*/
    function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->database();
    }

    /*----------------------------------------GET KONTAK----------------------------------------*/
    function index_get()
    {
        $this->db->distinct();
        $this->db->group_by('mac');
        #$this->db->select('mac', 'source');
        $data = $this->db->get('beacon')->result();

        if ($data) {
             $obj = array(
                'status' => 200,
                'data'=> $data
            );
        } else {
            $obj = array(
                'status' => 404,
                'data'=> $data
            );
        }
        echo json_encode($obj);
    }

    function index_post()
    {
        $mac = $this->post('mac');

        $this->db->where('mac', $mac);
        $this->db->limit(1);
        $this->db->order_by('nomor',"DESC");
        $data = $this->db->get('koordinat')->row();

        $this->db->where('mac', $mac);
        $this->db->limit(1);
        $this->db->order_by('nomor',"DESC");
        $data2 = $this->db->get('real')->row();

        if ($data) {
             $obj = array(
                'status' => 200,
                'formula'=> $data,
                'real'=> $data2
            );
        
        } else {
            $obj = array(
                'status' => 404,
                'data'=> $data
            );
        }
        echo json_encode($obj);  
    }
}