function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

let column = [
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
];

let y = 0;
let validateDVal = [
];

let validateEVal  = [
    'voice',
    'sms',
    'video',
    'mms_new',
    'mms_refund',
    'voice_int',
    'voice_mtc',
    'voice_roaming',
    'voice_ucb',
    'CollectCall',
    'CallAddict',
    'sms_init',
    'sms_terminate',
    'voice_init_new',
    'voice_terminate_new',
    'voice_intermediate_new'
];

let validateFVal = [
    'voice',
    'sms',
    'gprs',
    'video',
    'mms_new',
    'mms_refund',
    'voice_int',
    'voice_mtc',
    'voice_roaming',
    'voice_ucb',
    'CollectCall',
    'CallAddict',
    'gprs_location',
    'direct_debit_new',
    'content_refund_new',
    'talkmania',
    'talkmania_ifrs',
    'accumulator_ifrs',
    'addoffer',
    'addofferwithparameter',
    'addoffer_ou',
    'update_subscriber_add',
    'update_subscriber_remove',
    'removeoffer',
    'update_balance',
    'recharge',
    'transfer_balance',
    '888',
    '889',
    'checkoffer',
    'check_notif',
    'update_accumulator',
    'update_expdate',
    'update_parameter',
    'getbonusinfo',
    'getbalanceinfo',
    'create_post_sub',
    'create_post_sub',
    'createHyb',
    'create_ou_sub',
    'createGroup',
    'addGroupMember',
    'voice_init_new',
    'voice_intermediate_new',
    'voice_terminate_new',
    'sms_init',
    'sms_terminate',
    'gprs_init_payu',
    'gprs_intermediate_payu',
    'gprs_terminate_payu',
    'check_trb1_sub_errs',
    'change_pp',
    'get_charge_amdd_oc',
    'get_charge_amdd_rc',
    'check_rb_log',
    'check_tevt',
    'check_chg',
    'check_indira',
    'get_protocol_xml',
    'check_all_offer',
    'check_pritname',
    'check_RE_online',
    'check_RE_offline',
    'flu'
];

let validateGVal = [
    'voice_int',
    'voice_mtc',
    'voice_roaming',
    'voice_ucb',
    'voice_init_new',
    'voice_intermediate_new',
    'voice_terminate_new'
];

let validateHVal = [
    'voice_roaming',
    'voice_mtc',
    'video',
    'voice_ucb',
    'CollectCall',
    'CallAddict',
    'voice_init_new',
    'voice_intermediate_new',
    'voice_terminate_new'
];

let validateIVal = [
    'check_RE_online',
    'check_pritname',
    'check_all_offer',
    'get_protocol_xml',
    'check_tevt',
    'check_chg',
    'check_rb_log',
    'change_pp',
    'check_indira',
    'get_charge_amdd_oc',
    'get_charge_amdd_rc',
    'check_trb1_sub_errs',
    'direct_debit_new',
    'content_refund_new',
    'talkmania',
    'addoffer',
    'removeoffer',
    '889',
    'gprs_init_payu',
    'voice_init_new',
    'flu',
    'update_expdate',
    'create_post_sub',
    'create_post_sub',
    'getbonusinfo',
    'addofferwithparameter',
    'checkoffer',
    'createHyb',
    'getbalanceinfo',
    'create_ou_sub',
    'addoffer_ou',
    'sms_init',
    'update_parameter',
    'check_notif',
    'talkmania_ifrs',
    'accumulator_ifrs',
    'update_subscriber_add',
    'update_subscriber_remove',
    '888'
];

let validateJVal = [
    'gprs',
    'gprs_location',
    'gprs_init_payu',
    'gprs_intermediate_payu',
    'gprs_terminate_payu',
    'transfer_balance',
    'talkmania_ifrs',
    'voice_init_new',
    'voice_intermediate_new',
    'voice_terminate_new'
];

let validateKVal = [
    'gprs',
    'gprs_location',
    'gprs_init_payu',
    'gprs_intermediate_payu',
    'gprs_terminate_payu',
    'addofferwithparameter',
    'update_accumulator',
    'create_post_sub',
    'update_parameter',
    'talkmania_ifrs',
    'update_subscriber_add',
    'voice_init_new',
    'voice_intermediate_new',
    'voice_terminate_new'
];

let validateLVal = [
    'direct_debit_new',
    'content_refund_new',
    'gprs',
    'gprs_location',
    'gprs_init_payu',
    'gprs_intermediate_payu',
    'gprs_terminate_payu',
    'addofferwithparameter',
    'update_accumulator',
    'create_post_sub',
    'update_parameter',
    'update_subscriber_add'
];

let validateMVal = [
    'direct_debit_new',
    'content_refund_new',
    'talkmania',
    'addoffer',
    'removeoffer',
    'voice_init_new',
    'voice_intermediate_new',
    'voice_terminate_new',
    'gprs_init_payu',
    'gprs_intermediate_payu',
    'gprs_terminate_payu',
    'create_post_sub',
    'create_post_sub',
    'recharge',
    'addofferwithparameter',
    'update_accumulator',
    'checkoffer',
    'createHyb',
    'create_ou_sub',
    'addoffer_ou',
    'sms_init',
    'sms_terminate',
    'talkmania_ifrs',
    'update_subscriber_add',
    'update_subscriber_remove',
    'flu',
    'check_all_offer',
    'change_pp'
];

let validateNVal = [
    'direct_debit_new',
    'check_RE_offline',
    'content_refund_new',
    'mms_new',
    'mms_refund',
    'voice_intermediate_new',
    'voice_terminate_new',
    'gprs_intermediate_payu',
    'gprs_terminate_payu',
    'create_post_sub',
    'create_post_sub',
    'update_accumulator',
    'createHyb',
    'recharge',
    'create_ou_sub',
    'accumulator_ifrs',
    'update_subscriber_add',
    'update_subscriber_remove'
]

let validateSpaceBCondition1 = [
    "check_RE_online",
    "check_all_offer",
    "get_protocol_xml",
    "check_indira",
    "check_chg",
    "check_tevt",
    "check_rb_log",
    "get_charge_amdd_rc",
    "get_charge_amdd_oc",
    "check_trb1_sub_errs",
    "addoffer",
    "addoffer_ou",
    "createGroup",
    "addGroupMember",
    "update_accumulator",
    "checkoffer",
    "removeoffer",
    "create_post_sub",
    "create_post_sub",
    "createHyb",
    "create_OU",
    "update_expdate",
    "getbonusinfo",
    "accumulator_ifrs",
    "flu",
    "getbalanceinfo",
    "888",
    "889",
    "update_balance",
    "talkmania_ifrs",
    "update_subscriber_add",
    "update_subscriber_remove",
    "change_pp"
]

let validateSpaceBCondition2 = [
    "check_pritname",
    "update_parameter",
    "check_notif",
    "addofferwithparameter"
]

let validateSpaceBCondition21 = [
    "check_RE_online",
    "check_all_offer",
    "get_protocol_xml",
    "check_indira",
    "check_chg",
    "check_tevt",
    "check_rb_log",
    "get_charge_amdd_rc",
    "get_charge_amdd_oc",
    "check_trb1_sub_errs",
    "addoffer",
    "addoffer_ou",
    "createGroup",
    "addGroupMember",
    "update_accumulator",
    "checkoffer",
    "removeoffer",
    "create_post_sub",
    "create_post_sub",
    "createHyb",
    "create_OU",
    "update_expdate",
    "getbonusinfo",
    "accumulator_ifrs",
    "flu",
    "getbalanceinfo",
    "888",
    "889",
    "update_balance",
    "talkmania_ifrs",
    "update_subscriber_add",
    "update_subscriber_remove",
    "change_pp",
    "check_pritname",
    "update_parameter",
    "check_notif",
    "addofferwithparameter"
]

let validateSpaceBCondition31 = [
    "check_RE_online",
    "check_all_offer",
    "get_protocol_xml",
    "check_indira",
    "check_chg",
    "check_tevt",
    "check_rb_log",
    "get_charge_amdd_rc",
    "get_charge_amdd_oc",
    "check_trb1_sub_errs",
    "createGroup",
    "addGroupMember",
    "update_accumulator",
    "checkoffer",
    "removeoffer",
    "create_post_sub",
    "create_post_sub",
    "createHyb",
    "create_OU",
    "update_expdate",
    "getbonusinfo",
    "accumulator_ifrs",
    "flu",
    "getbalanceinfo",
    "888",
    "889",
    "update_balance",
    "change_pp",
    "check_pritname",
    "update_parameter",
    "check_notif"
]

let validateSpaceBCondition32 = [
    "addoffer",
    "addoffer_ou",
    "addofferwithparameter",
    "update_accumulator",
    "talkmania_ifrs",
    "update_subscriber_add",
    "update_subscriber_remove"
]


let optionSelectForm = "<option value=''>PIlih Type Event</option>"+
                        "<option value='voice'>Voice</option>"+
                        "<option value='sms'>SMS</option>"+
                        "<option value='gprs'>GPRS</option>"+
                        "<option value='mms_new'>MMS</option>"+
                        "<option value='mms_refund'>MMS Refund</option>"+
                        "<option value='voice_int'>Voice_IDD</option>"+
                        "<option value='voice_mtc'>Voice_MTC</option>"+
                        "<option value='voice_roaming'>Voice_Roaming</option>"+
                        "<option value='voice_ucb'>Voice_UCB</option>"+
                        "<option value='CollectCall'>Voice_Collect</option>"+
                        "<option value='CallAddict'>Voice_Addict</option>"+
                        "<option value='gprs_location'>GPRS_Location</option>"+
                        "<option value='direct_debit_new'>Direct_Debit</option>"+
                        "<option value='content_refund_new'>Content_Refund</option>"+
                        "<option value='talkmania'>Talk_Mania</option>"+
                        "<option value='talkmania_ifrs'>Talk_Mania_IFRS</option>"+
                        "<option value='accumulator_ifrs'>Accumulator_IFRS</option>"+
                        "<option value='addoffer'>Attach_Offer</option>"+
                        "<option value='addofferwithparameter'>Att_Offer_wParam</option>"+
                        "<option value='addoffer_ou'>Attach_Offer_OU</option>"+
                        "<option value='update_subscriber_add'>UpdateSubs_Attach</option>"+
                        "<option value='update_subscriber_remove'>UpdateSubs_Remove</option>"+
                        "<option value='removeoffer'>Remove_Offer</option>"+
                        "<option value='update_balance'>Update_Balance</option>"+
                        "<option value='recharge'>Recharge</option>"+
                        "<option value='transfer_balance'>Transfer_Balance</option>"+
                        "<option value='flu'>FLU</option>"+
                        "<option value='888'>888</option>"+
                        "<option value='889'>889</option>"+
                        "<option value='checkoffer'>Check_Offer</option>"+
                        "<option value='check_notif'>Check_Notification</option>"+
                        "<option value='update_accumulator'>Update_Accumulator</option>"+
                        "<option value='update_expdate'>Update_ExpDate</option>"+
                        "<option value='update_parameter'>Update_Parameter</option>"+
                        "<option value='getbonusinfo'>GetBonusInfo</option>"+
                        "<option value='getbalanceinfo'>GetBalanceInfo</option>"+
                        "<option value='create_post_sub'>Create_Postpaid</option>"+
                        "<option value='create_post_sub'>Create_Postpaid_new</option>"+
                        "<option value='createHyb'>Create_Hybrid</option>"+
                        "<option value='create_ou_sub'>Create_OU</option>"+
                        "<option value='createGroup'>Create_CUG_Group</option>"+
                        "<option value='addGroupMember'>Add_CUG_Member</option>"+
                        "<option value='voice_init_new'>Voice Initial</option>"+
                        "<option value='voice_intermediate_new'>Voice Intermediate</option>"+
                        "<option value='voice_terminate_new'>Voice Terminate</option>"+
                        "<option value='sms_init'>SMS Initial</option>"+
                        "<option value='sms_terminate'>SMS Terminate</option>"+
                        "<option value='gprs_init_payu'>GPRS Initial</option>"+
                        "<option value='gprs_intermediate_payu'>GPRS Intermediate</option>"+
                        "<option value='gprs_terminate_payu'>GPRS Terminate</option>"+
                        "<option value='check_trb1_sub_errs'>Check TRB1_SUB_ERRS</option>"+
                        "<option value='change_pp'>Change_PP</option>"+
                        "<option value='get_charge_amdd_oc'>Get OfferCharge_AMDDcode_OC</option>"+
                        "<option value='get_charge_amdd_rc'>Get OfferCharge_AMDDcode_RC</option>"+
                        "<option value='check_rb_log'>Check RB_Log</option>"+
                        "<option value='check_tevt'>Check TEVT by OfferID</option>"+
                        "<option value='check_chg'>Check CHG by Transaction</option>"+
                        "<option value='check_indira'>Check Indira</option>"+
                        "<option value='get_protocol_xml'>Get Protocol_XML</option>"+
                        "<option value='check_all_offer'>Check All_Offer</option>"+
                        "<option value='check_pritname'> Check_PritName</option>"+
                        "<option value='check_RE_online'>Check_RE_Online</option>"+
                        "<option value='check_RE_offline'>Check_RE_Offline</option>";

let optionSelectForm2 = "<option value='889'>889</option><option value='889*1'>889*1</option><option value='889*2'>889*2</option><option value='889*3'>889*3</option><option value='889*4'>889*4</option><option value='889*5'>889*5</option>";

function addRow(rowNumber){
    var tableHeader = '<tr>'+
                    '<th id="A" class="modal-show" data-modal="A">No</th>'+
                    '<th id="B" class="modal-show" data-modal="B">Type Event</th>'+
                    '<th id="C" class="modal-show" data-modal="C">Desc</th>'+
                    '<th id="D" class="modal-show" data-modal="D">A number</th>'+
                    '<th id="E" class="modal-show" data-modal="E">B number</th>'+
                    '<th id="F" class="modal-show" data-modal="F">CGI</th>'+
                    '<th id="G" class="modal-show" data-modal="G">Access Code</th>'+
                    '<th id="H" class="modal-show" data-modal="H">VLR ID</th>'+
                    '<th id="I" class="modal-show" data-modal="I">Duration/CC-Unit /Amount/CUG Name</th>'+
                    '<th id="J" class="modal-show" data-modal="J">APN/ TransferParty/ProdID|TrxID/ ServiceType</th>'+
                    '<th id="K" class="modal-show" data-modal="K">MCC_MNC/ParamName/Action/Customer_Type/Allow_Item_Level_Cost/ ServiceSubType</th>'+
                    '<th id="L" class="modal-show" data-modal="L">Rating Group/ ParamValue/ UOM/ContentID/ Customer_sub_type</th>'+
                    '<th id="M" class="modal-show" data-modal="M">Vas Code/Cost Code/Offer/SessionID/PricePlan/SplitCode/SOC CD</th>'+
                    '<th id="N" class="modal-show" data-modal="N">Modifier3/IMSI/CC-Req Number/BillCycle/ ItemId|DimensionID /ChannelRecharge</th>'+
                    '<th id="O" class="modal-show" data-modal="O">Expected Charge</th>'+
                    '<th id="P" class="modal-show" data-modal="P">Bonus Check</th>'+
                    '<th id="Q" class="modal-show" data-modal="Q">Date/Day</th>'+
                    '<th id="R" class="modal-show" data-modal="R">Hour-minutes-second</th>'+
                    '<th id="S" class="modal-show" data-modal="S">Timestamp</th>'+
                    '<th id="T" class="modal-show" data-modal="T">889 Option</th>'+
                    '<th id="U" class="modal-show" data-modal="U">Option Automation</th>'+
                    '<th id="V" class="modal-show" data-modal="V">Action</th>'
                '</tr>';
    
    var text = "";
    var textbawah = "";
    for (var i = 1; i <= rowNumber; i++) {
        text += "<tr>"+
        "<th class='nomer' id='A"+i+"'>"+i+"</th>"+
        "<th><select class='typeevent"+i+" select1' name='B' id='B"+i+"' data-row="+i+">"+optionSelectForm+"</select></th>"+
        "<th><textarea class='input' name='C' id='C"+i+"' style='background: yellow;'></textarea></th>"+
        "<th><input type='text' class='input' name='D' id='D"+i+"' data-row="+i+"></th>"+
        "<th><input type='text' class='input' name='E' id='E"+i+"' data-row="+i+"></th>"+
        "<th><input type='text' class='input' name='F' id='F"+i+"' data-row="+i+"></th>"+
        "<th><input type='text' class='input' name='G' id='G"+i+"' data-row="+i+"></th>"+
        "<th><input type='text' class='input' name='H' id='H"+i+"' data-row="+i+"></th>"+
        "<th><input type='text' class='input' name='I' id='I"+i+"' data-row="+i+"></th>"+
        "<th><input type='text' class='input' name='J' id='J"+i+"' data-row="+i+"></th>"+
        "<th><input type='text' class='input' name='K' id='K"+i+"' data-row="+i+"></th>"+
        "<th><input type='text' class='input' name='L' id='L"+i+"' data-row="+i+"></th>"+
        "<th><input type='text' class='input' name='M' id='M"+i+"' data-row="+i+"></th>"+
        "<th><input type='text' class='input' name='N' id='N"+i+"' data-row="+i+"></th>"+
        "<th><input type='text' class='input' name='O' id='O"+i+"' style='background: lightskyblue;' data-row="+i+" ></th>"+
        "<th><input type='text' class='input' name='P' id='P"+i+"' style='background: lightskyblue;' data-row="+i+"></th>"+
        "<th><input type='text' class='input' name='Q' id='Q"+i+"' style='background: yellow;' data-row="+i+"></th>"+
        "<th><input type='text' class='input' name='R' id='R"+i+"' style='background: yellow;' data-row="+i+"></th>"+
        "<th><input type='text' class='input' name='S' id='S"+i+"' style='background: gray;' data-row="+i+"></th>"+
        "<th><select class='select' name='T' id='T"+i+"' data-row="+i+">"+optionSelectForm2+"</select></th>"+
        "<th><select class='select' name='U' id='U"+i+"' data-row="+i+"><option value='Log'>Log</option><option value='Query_Balance'>Query Balance</option></select></th>"+
        "<th style='display:flex;flex-direction:row;'><input type='button' value='Delete Row' id='RemoveRow'/><input type='button' value='Add Row' id='ActionAdd'/></th>"
        "</tr>";
        textbawah +="<tr class='bg-success'>"+
                        "<td><span id='valA"+i+"'></i></td>"+
                    "</tr>"+
                    "<tr class='bg-success'>"+
                        "<td><span id='valB"+i+"'></i></td>"+
                    "</tr>"+
                    "<tr class='bg-success'>"+
                        "<td><span id='valC"+i+"'></i></td>"+
                    "</tr>"+
                    "<tr class='bg-success'>"+
                        "<td><span id='valD"+i+"'></i></td>"+
                    "</tr>"+
                    "<tr class='bg-success'>"+
                        "<td><span id='valE"+i+"'></i></td>"+
                    "</tr>"+
                    "<tr class='bg-success'>"+
                        "<td><span id='valF"+i+"'></i></td>"+
                    "</tr>";
        y++
    }

    $('#trjudul').html(tableHeader);
    $("#demo").html(text);
    $('#bodybawah').html(textbawah);
}

function validateForm(x){
    let bVal = $('#B'+x).val();

    if($.inArray(bVal,validateDVal) != -1){
        document.getElementById('D'+x).style.backgroundColor='yellow';
    }else{
        document.getElementById('D'+x).style.backgroundColor=null;
    }
        
    if($.inArray(bVal,validateEVal) != -1)
    {
        document.getElementById("E"+x).style.backgroundColor='yellow';
    }else{
        document.getElementById("E"+x).style.backgroundColor=null;
    }

    if($.inArray(bVal,validateFVal) != -1)
    {
        document.getElementById("F"+x).style.backgroundColor='yellow'
    }

    if($.inArray(bVal,validateGVal) != -1)
    {
        document.getElementById('G'+x).style.backgroundColor='yellow';
    }else{
        document.getElementById('G'+x).style.backgroundColor=null;
    }

    if($.inArray(bVal,validateHVal) != -1)
    {
        document.getElementById("H"+x).style.backgroundColor='yellow';
    }else{
        document.getElementById("H"+x).style.backgroundColor=null;
    }
    
    if($.inArray(bVal,validateIVal) != -1)
    {
        document.getElementById("I"+x).style.backgroundColor=null;
    }else{
        document.getElementById("I"+x).style.backgroundColor='yellow';
    }
    
    if($.inArray(bVal,validateJVal) != -1)
    {
        document.getElementById("J"+x).style.backgroundColor='yellow';
    }else{
        document.getElementById("J"+x).style.backgroundColor=null;
    }
    
    if($.inArray(bVal,validateKVal) != -1)
    {
        document.getElementById("K"+x).style.backgroundColor='yellow';
    }else{
        document.getElementById("K"+x).style.backgroundColor=null;
    }
    
    if($.inArray(bVal,validateLVal) != -1)
    {
        document.getElementById("L"+x).style.backgroundColor='yellow';
    }else{
        document.getElementById("L"+x).style.backgroundColor=null;
    }
    
    if($.inArray(bVal,validateMVal) != -1)
    {
        document.getElementById("M"+x).style.backgroundColor='yellow';
    }else{
        document.getElementById("M"+x).style.backgroundColor=null;
    }
    
    if($.inArray(bVal,validateNVal) != -1)
    {
        document.getElementById("N"+x).style.backgroundColor='yellow';
    }else{
        document.getElementById("N"+x).style.backgroundColor=null;
    }
}

function validateFormDisable(x){
    $(document).on('click','#get-data',function(){
        $('#valA'+x).html('#'+x+'.~<i id="text_uuidA'+x+'"></i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+']')
        $('#text_uuidA'+x).text(UUID);
        $('input[id="S'+x+'"]').val(','+document.getElementById("Q"+x).value+'-'+document.getElementById("R"+x).value)
    });
    if(document.getElementById("B"+x).value == "voice"){
        $('#D'+x).prop("disabled", true)
        $('#E'+x).prop("disabled", false)
        $('#F'+x).prop("disabled", false)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", false)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' 1,'+document.getElementById("D"+x).value+',+'+document.getElementById("E"+x).value+','+document.getElementById("F"+x).value+',0,'+document.getElementById("I"+x).value+document.getElementById("S"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "flu"){
        $('#D'+x).prop("disabled", true)
        $('#E'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", false)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' 1,'+document.getElementById("M"+x).value+','+document.getElementById("F"+x).value+document.getElementById("S"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "sms"){
        $('#D'+x).prop("disabled", true)
        $('#E'+x).prop("disabled", false)
        $('#F'+x).prop("disabled", false)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", false)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' 1,'+document.getElementById("D"+x).value+',+'+document.getElementById("E"+x).value+','+document.getElementById("F"+x).value+','+document.getElementById("I"+x).value+document.getElementById("S"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "gprs"){
        $('#D'+x).prop("disabled", true)
        $('#E'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", false)
        $('#J'+x).prop("disabled", false)
        $('#K'+x).prop("disabled", false)
        $('#L'+x).prop("disabled", false)
        $('#M'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' 2,'+document.getElementById("D"+x).value+','+document.getElementById("J"+x).value+','+document.getElementById("K"+x).value+','+document.getElementById("L"+x).value+','+document.getElementById("I"+x).value+document.getElementById("S"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "video"){
        $('#D'+x).prop("disabled", true)
        $('#E'+x).prop("disabled", false)
        $('#F'+x).prop("disabled", false)
        $('#H'+x).prop("disabled", false)
        $('#I'+x).prop("disabled", false)
        $('#G'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' 1,'+document.getElementById("D"+x).value+',+'+document.getElementById("E"+x).value+','+document.getElementById("F"+x).value+',10,'+document.getElementById("H"+x).value+','+document.getElementById("I"+x).value+document.getElementById("S"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "mms_new"){
        $('#D'+x).prop("disabled", true)
        $('#E'+x).prop("disabled", false)
        $('#F'+x).prop("disabled", false)
        $('#I'+x).prop("disabled", false)
        $('#N'+x).prop("disabled", false)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' 2,'+document.getElementById("D"+x).value+','+document.getElementById("I"+x).value+',+'+document.getElementById("E"+x).value+','+document.getElementById("N"+x).value+','+document.getElementById("F"+x).value+document.getElementById("S"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "mms_refund"){
        $('#D'+x).prop("disabled", true)
        $('#E'+x).prop("disabled", false)
        $('#F'+x).prop("disabled", false)
        $('#I'+x).prop("disabled", false)
        $('#N'+x).prop("disabled", false)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' 2,'+document.getElementById("D"+x).value+','+document.getElementById("I"+x).value+',+'+document.getElementById("E"+x).value+','+document.getElementById("N"+x).value+','+document.getElementById("F"+x).value+document.getElementById("S"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "voice_int"){
        $('#D'+x).prop("disabled", true)
        $('#E'+x).prop("disabled", false)
        $('#F'+x).prop("disabled", false)
        $('#G'+x).prop("disabled", false)
        $('#I'+x).prop("disabled", false)
        $('#H'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' 1,'+document.getElementById("D"+x).value+','+document.getElementById("E"+x).value+','+document.getElementById("F"+x).value+','+document.getElementById("G"+x).value+','+document.getElementById("I"+x).value+document.getElementById("S"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "voice_mtc"){
        $('#D'+x).prop("disabled", true)
        $('#E'+x).prop("disabled", false)
        $('#F'+x).prop("disabled", false)
        $('#G'+x).prop("disabled", false)
        $('#H'+x).prop("disabled", false)
        $('#I'+x).prop("disabled", false)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' 1,'+document.getElementById("D"+x).value+',+'+document.getElementById("E"+x).value+','+document.getElementById("F"+x).value+','+document.getElementById("G"+x).value+','+document.getElementById("H"+x).value+','+document.getElementById("I"+x).value+document.getElementById("S"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "voice_roaming"){
        $('#D'+x).prop("disabled", true)
        $('#E'+x).prop("disabled", false)
        $('#F'+x).prop("disabled", false)
        $('#G'+x).prop("disabled", false)
        $('#H'+x).prop("disabled", false)
        $('#I'+x).prop("disabled", false)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' 1,'+document.getElementById("D"+x).value+',+'+document.getElementById("E"+x).value+','+document.getElementById("F"+x).value+','+document.getElementById("G"+x).value+','+document.getElementById("H"+x).value+','+document.getElementById("I"+x).value+document.getElementById("S"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "voice_ucb"){
        $('#D'+x).prop("disabled", true)
        $('#E'+x).prop("disabled", false)
        $('#F'+x).prop("disabled", false)
        $('#G'+x).prop("disabled", false)
        $('#H'+x).prop("disabled", false)
        $('#I'+x).prop("disabled", false)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' 1,'+document.getElementById("D"+x).value+',+'+document.getElementById("E"+x).value+','+document.getElementById("F"+x).value+','+document.getElementById("G"+x).value+','+document.getElementById("H"+x).value+','+document.getElementById("I"+x).value+document.getElementById("S"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "CollectCall"){
        $('#D'+x).prop("disabled", true)
        $('#E'+x).prop("disabled", false)
        $('#F'+x).prop("disabled", false)
        $('#H'+x).prop("disabled", false)
        $('#I'+x).prop("disabled", false)
        $('#G'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' 1,'+document.getElementById("D"+x).value+',+'+document.getElementById("E"+x).value+','+document.getElementById("F"+x).value+',10,'+document.getElementById("H"+x).value+','+document.getElementById("I"+x).value+document.getElementById("S"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "CallAddict"){  
        $('#D'+x).prop("disabled", true)
        $('#E'+x).prop("disabled", false)
        $('#F'+x).prop("disabled", false)
        $('#H'+x).prop("disabled", false)
        $('#I'+x).prop("disabled", false)
        $('#G'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' 1,'+document.getElementById("D"+x).value+',+'+document.getElementById("E"+x).value+','+document.getElementById("F"+x).value+',10,'+document.getElementById("H"+x).value+','+document.getElementById("I"+x).value+document.getElementById("S"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "gprs_location"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#I'+x).prop("disabled", false)
        $('#J'+x).prop("disabled", false)
        $('#K'+x).prop("disabled", false)
        $('#K'+x).prop("disabled", false)
        $('#L'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' 2,'+document.getElementById("D"+x).value+','+document.getElementById("J"+x).value+','+document.getElementById("K"+x).value+','+document.getElementById("L"+x).value+','+document.getElementById("F"+x).value+','+document.getElementById("I"+x).value+document.getElementById("S"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "direct_debit_new"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#L'+x).prop("disabled", false)
        $('#M'+x).prop("disabled", false)
        $('#N'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' 2,'+document.getElementById("D"+x).value+','+document.getElementById("M"+x).value+','+document.getElementById("N"+x).value+'|'+document.getElementById("L"+x).value+','+document.getElementById("F"+x).value+document.getElementById("S"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "content_refund_new"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#L'+x).prop("disabled", false)
        $('#M'+x).prop("disabled", false)
        $('#N'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' 2,'+document.getElementById("D"+x).value+','+document.getElementById("M"+x).value+','+document.getElementById("N"+x).value+'|'+document.getElementById("L"+x).value+','+document.getElementById("F"+x).value+document.getElementById("S"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "talkmania"){     
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#M'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' 2,'+document.getElementById("D"+x).value+','+document.getElementById("M"+x).value+document.getElementById("S"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "talkmania_ifrs"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#J'+x).prop("disabled", false)
        $('#K'+x).prop("disabled", false)
        $('#M'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)        
        $('#L'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' 2,'+document.getElementById("D"+x).value+','+document.getElementById("M"+x).value+','+document.getElementById("J"+x).value+','+document.getElementById("K"+x).value+',Futurestring')
        });
    }else if(document.getElementById("B"+x).value == "accumulator_ifrs"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#N'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' '+document.getElementById("D"+x).value+','+document.getElementById("N"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "addoffer"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#M'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' '+document.getElementById("D"+x).value+','+document.getElementById("M"+x).value+',n')
        });
    }else if(document.getElementById("B"+x).value == "addofferwithparameter"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#K'+x).prop("disabled", false)
        $('#L'+x).prop("disabled", false)
        $('#M'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' '+document.getElementById("D"+x).value+','+document.getElementById("M"+x).value+','+document.getElementById("K"+x).value+','+document.getElementById("L"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "addoffer_ou"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#M'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' '+document.getElementById("D"+x).value+','+document.getElementById("M"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "update_subscriber_add"){
        $('#D'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", false)
        $('#F'+x).prop("disabled", false)
        $('#L'+x).prop("disabled", false)
        $('#N'+x).prop("disabled", false)
        $('#M'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' '+document.getElementById("D"+x).value+','+document.getElementById("M"+x).value+','+document.getElementById("N"+x).value+','+document.getElementById("K"+x).value+','+document.getElementById("L"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "update_subscriber_remove"){
        $('#D'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", false)
        $('#F'+x).prop("disabled", false)
        $('#N'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' '+document.getElementById("D"+x).value+','+document.getElementById("M"+x).value+','+document.getElementById("N"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "removeoffer"){
        $('#D'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", false)
        $('#F'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' '+document.getElementById("D"+x).value+','+document.getElementById("M"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "update_balance"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#I'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' '+document.getElementById("D"+x).value+','+document.getElementById("I"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "transfer_balance"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#I'+x).prop("disabled", false)
        $('#J'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' '+document.getElementById("D"+x).value+','+document.getElementById("I"+x).value+','+document.getElementById("J"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "888"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' 1,'+document.getElementById("D"+x).value+','+document.getElementById("F"+x).value+',0')
        });
    }else if(document.getElementById("B"+x).value == "889"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' 1,'+document.getElementById("D"+x).value+','+document.getElementById("F"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "checkoffer"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", false)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' ,'+document.getElementById("M"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "check_notif"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' ,')
        });
    }else if(document.getElementById("B"+x).value == "update_accumulator"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#I'+x).prop("disabled", false)
        $('#K'+x).prop("disabled", false)
        $('#L'+x).prop("disabled", false)
        $('#M'+x).prop("disabled", false)
        $('#N'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' '+document.getElementById("D"+x).value+','+document.getElementById("I"+x).value+','+document.getElementById("K"+x).value+','+document.getElementById("L"+x).value+','+document.getElementById("M"+x).value+','+document.getElementById("N"+x).value+document.getElementById("S"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "update_expdate"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' '+document.getElementById("D"+x).value+','+document.getElementById("Q"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "update_parameter"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#K'+x).prop("disabled", false)
        $('#L'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' '+document.getElementById("D"+x).value+','+document.getElementById("K"+x).value+','+document.getElementById("L"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "getbonusinfo"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' '+document.getElementById("D"+x).value+',')
        });
    }else if(document.getElementById("B"+x).value == "getbalanceinfo"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' '+document.getElementById("D"+x).value+',')
        });
    }else if(document.getElementById("B"+x).value == "create_post_sub"){
        $('#D'+x).prop("disabled", true)
        $('#E'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#K'+x).prop("disabled", false)
        $('#L'+x).prop("disabled", false)
        $('#M'+x).prop("disabled", false)
        $('#N'+x).prop("disabled", false)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' '+document.getElementById("D"+x).value+','+document.getElementById("M"+x).value+','+document.getElementById("N"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "createHyb"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#M'+x).prop("disabled", false)
        $('#N'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' '+document.getElementById("D"+x).value+','+document.getElementById("M"+x).value+','+document.getElementById("N"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "create_ou_sub"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#M'+x).prop("disabled", false)
        $('#N'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' '+document.getElementById("D"+x).value+','+document.getElementById("F"+x).value+','+document.getElementById("M"+x).value+','+document.getElementById("N"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "createGroup"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#I'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' '+document.getElementById("I"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "addGroupMember"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#I'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' '+document.getElementById("D"+x).value+','+document.getElementById("I"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "voice_init_new"){
        $('#D'+x).prop("disabled", true)
        $('#E'+x).prop("disabled", false)
        $('#F'+x).prop("disabled", false)
        $('#G'+x).prop("disabled", false)
        $('#H'+x).prop("disabled", false)
        $('#J'+x).prop("disabled", false)
        $('#K'+x).prop("disabled", false)
        $('#M'+x).prop("disabled", false)
        $('#I'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' 1,'+document.getElementById("M"+x).value+',91u,'+document.getElementById("D"+x).value+','+document.getElementById("J"+x).value+','+document.getElementById("K"+x).value+','+document.getElementById("D"+x).value+',+'+document.getElementById("E"+x).value+','+document.getElementById("F"+x).value+','+document.getElementById("H"+x).value+','+document.getElementById("G"+x).value+',0'+document.getElementById("S"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "voice_intermediate_new"){
        $('#D'+x).prop("disabled", true)
        $('#E'+x).prop("disabled", false)
        $('#F'+x).prop("disabled", false)
        $('#G'+x).prop("disabled", false)
        $('#H'+x).prop("disabled", false)
        $('#J'+x).prop("disabled", false)
        $('#K'+x).prop("disabled", false)
        $('#M'+x).prop("disabled", false)
        $('#I'+x).prop("disabled", false)
        $('#N'+x).prop("disabled", false)
        $('#L'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' 1,'+document.getElementById("M"+x).value+',92u,'+document.getElementById("N"+x).value+','+document.getElementById("D"+x).value+','+document.getElementById("J"+x).value+','+document.getElementById("K"+x).value+','+document.getElementById("D"+x).value+',+'+document.getElementById("E"+x).value+','+document.getElementById("F"+x).value+','+document.getElementById("H"+x).value+','+document.getElementById("G"+x).value+',0,'+document.getElementById("I"+x).value+document.getElementById("S"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "voice_terminate_new"){
        $('#D'+x).prop("disabled", true)
        $('#E'+x).prop("disabled", false)
        $('#F'+x).prop("disabled", false)
        $('#G'+x).prop("disabled", false)
        $('#H'+x).prop("disabled", false)
        $('#I'+x).prop("disabled", false)
        $('#J'+x).prop("disabled", false)
        $('#K'+x).prop("disabled", false)
        $('#N'+x).prop("disabled", false)
        $('#M'+x).prop("disabled", false)
        $('#L'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' 1,'+document.getElementById("M"+x).value+',93u,'+document.getElementById("N"+x).value+','+document.getElementById("D"+x).value+','+document.getElementById("J"+x).value+','+document.getElementById("K"+x).value+','+document.getElementById("D"+x).value+',+'+document.getElementById("E"+x).value+','+document.getElementById("F"+x).value+','+document.getElementById("H"+x).value+','+document.getElementById("G"+x).value+',0,'+document.getElementById("I"+x).value+document.getElementById("S"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "sms_init"){
        $('#D'+x).prop("disabled", true)
        $('#E'+x).prop("disabled", false)
        $('#F'+x).prop("disabled", false)
        $('#M'+x).prop("disabled", false)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' 1,'+document.getElementById("M"+x).value+','+document.getElementById("D"+x).value+',+'+document.getElementById("E"+x).value+','+document.getElementById("F"+x).value+document.getElementById("S"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "sms_terminate"){
        $('#D'+x).prop("disabled", true)
        $('#E'+x).prop("disabled", false)
        $('#F'+x).prop("disabled", false)
        $('#I'+x).prop("disabled", false)
        $('#M'+x).prop("disabled", false)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' 1,'+document.getElementById("M"+x).value+','+document.getElementById("D"+x).value+',+'+document.getElementById("E"+x).value+','+document.getElementById("F"+x).value+','+document.getElementById("I"+x).value+document.getElementById("S"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "gprs_init_payu"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#K'+x).prop("disabled", false)
        $('#L'+x).prop("disabled", false)
        $('#M'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' 2,'+document.getElementById("D"+x).value+','+document.getElementById("M"+x).value+',0,'+document.getElementById("J"+x).value+','+document.getElementById("K"+x).value+','+document.getElementById("L"+x).value+','+document.getElementById("F"+x).value+document.getElementById("S"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "gprs_intermediate_payu"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#I'+x).prop("disabled", false)
        $('#J'+x).prop("disabled", false)
        $('#K'+x).prop("disabled", false)
        $('#L'+x).prop("disabled", false)
        $('#M'+x).prop("disabled", false)
        $('#N'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' 2,'+document.getElementById("D"+x).value+','+document.getElementById("M"+x).value+','+document.getElementById("J"+x).value+','+document.getElementById("K"+x).value+','+document.getElementById("L"+x).value+','+document.getElementById("I"+x).value+','+document.getElementById("F"+x).value+document.getElementById("S"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "gprs_terminate_payu"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#I'+x).prop("disabled", false)
        $('#J'+x).prop("disabled", false)
        $('#K'+x).prop("disabled", false)
        $('#L'+x).prop("disabled", false)
        $('#M'+x).prop("disabled", false)
        $('#N'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
             $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' 2,'+document.getElementById("D"+x).value+','+document.getElementById("M"+x).value+','+document.getElementById("J"+x).value+','+document.getElementById("K"+x).value+','+document.getElementById("L"+x).value+','+document.getElementById("I"+x).value+','+document.getElementById("F"+x).value+document.getElementById("S"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "check_trb1_sub_errs"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' '+document.getElementById("D"+x).value+','+document.getElementById("F"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "change_pp"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#M'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' '+document.getElementById("D"+x).value+','+document.getElementById("M"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "get_charge_amdd_oc"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' '+document.getElementById("D"+x).value+','+document.getElementById("F"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "get_charge_amdd_rc"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' '+document.getElementById("D"+x).value+','+document.getElementById("F"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "check_rb_log"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' '+document.getElementById("D"+x).value+','+document.getElementById("F"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "check_tevt"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' '+document.getElementById("D"+x).value+','+document.getElementById("F"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "check_chg"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' '+document.getElementById("D"+x).value+','+document.getElementById("F"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "check_indira"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' '+document.getElementById("D"+x).value+','+document.getElementById("F"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "get_protocol_xml"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' '+document.getElementById("D"+x).value+','+document.getElementById("F"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "check_all_offer"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#M'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' '+document.getElementById("D"+x).value+','+document.getElementById("M"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "check_pritname"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' '+document.getElementById("D"+x).value+','+document.getElementById("F"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "check_RE_online"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", true)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' '+document.getElementById("D"+x).value+','+document.getElementById("F"+x).value)
        });
    }else if(document.getElementById("B"+x).value == "check_RE_offline"){
        $('#D'+x).prop("disabled", true)
        $('#F'+x).prop("disabled", false)
        $('#E'+x).prop("disabled", true)
        $('#G'+x).prop("disabled", true)
        $('#H'+x).prop("disabled", true)
        $('#I'+x).prop("disabled", false)
        $('#J'+x).prop("disabled", true)
        $('#K'+x).prop("disabled", true)
        $('#L'+x).prop("disabled", true)
        $('#M'+x).prop("disabled", true)
        $('#N'+x).prop("disabled", true)
        $(document).on('click','#get-data',function(){
            $('#valB'+x).html(x+'.~<i id="text_uuidB'+x+'">'+UUID+'</i>~'+document.getElementById("C"+x).value+'['+document.getElementById("O"+x).value+'] '+document.getElementById("B"+x).value+' '+document.getElementById("D"+x).value+','+document.getElementById("F"+x).value+','+document.getElementById("I"+x).value)
        });
    }
}

function validateBChange(x){
    let bValChange = $("#B"+x+" option:selected").val();
    if($.inArray(bValChange,validateSpaceBCondition1) != -1){
        $(document).on('click','#get-data',function(){
            $('#valC'+x).text("space");
        });
    }else if($.inArray(bValChange,validateSpaceBCondition2) != -1){
        $(document).on('click','#get-data',function(){
            $('#valC'+x).text("#ReservedForDB");
        });
    }else{
        $(document).on('click','#get-data',function(){
            $('#valC'+x).html("~<i id='text_uuidC"+x+"'>"+UUID+"</i>~888 888 1 "+document.getElementById("D"+x).value+','+document.getElementById("F"+x).value+',0'+document.getElementById("S"+x).value);
        });
    }

    if($.inArray(bValChange,validateSpaceBCondition21) != -1){
        $(document).on('click','#get-data',function(){
            $('#valD'+x).text("space");
        });
    }else{
        $(document).on('click','#get-data',function(){
            $('#valD'+x).html("~<i id='text_uuidD"+x+"'>"+UUID+"</i>~887 887 1 "+document.getElementById("D"+x).value+','+document.getElementById("F"+x).value+',0'+document.getElementById("S"+x).value);
        });
    }

    if($.inArray(bValChange,validateSpaceBCondition31) != -1){
        $(document).on('click','#get-data',function(){
            $('#valE'+x).text("space")
        })
    }else if($.inArray(bValChange,validateSpaceBCondition32) != -1){
        $(document).on('click','#get-data',function(){
            $('#valE'+x).html("~<i id='text_uuidE"+x+"'>"+UUID+"</i>~GetBonusInfo getbonusinfo["+document.getElementById("O"+x).value+"] ,")
        })
    }else{
        $(document).on('click','#get-data',function(){
            $('#valE'+x).html("~<i id='text_uuidE"+x+"'>"+UUID+"</i>~"+document.getElementById("T"+x).value+'['+document.getElementById("O"+x).value+'] 889 1 '+document.getElementById("D"+x).value+','+document.getElementById("F"+x).value+',0'+document.getElementById("S"+x).value)
        })
    }

    $(document).on('click','#get-data',function(){
        $('#valF'+x).text("space_end")
    })
}

// var tulisan = "";
// Button GET NEW UUID
var UUID = uuidv4();
$('#getUUID').on('click',function (){
    var leng = document.getElementById("banyak").value;
    
    var UUID = uuidv4();
    for (let x = 1; x <= leng; x++) {
        $('#text_uuidA'+x).text(UUID);
        $('#text_uuidB'+x).text(UUID);
        $('#text_uuidC'+x).text(UUID);
        $('#text_uuidD'+x).text(UUID);
        $('#text_uuidE'+x).text(UUID);
    }
});

// SAVE INPUT
$('#save').click(function(){
    jsonObj = [];
    // $('select[name="B"]').each(function(index){
    for (let index = 1; index <= y; index++) {
        var B = $('select[id="B'+index+'"]').val();
        var C = $('textarea[id="C'+index+'"]').val();
        var D = $('input[id="D'+index+'"]').val();
        var E = $('input[id="E'+index+'"]').val();
        var F = $('input[id="F'+index+'"]').val();
        var G = $('input[id="G'+index+'"]').val();
        var H = $('input[id="H'+index+'"]').val();
        var I = $('input[id="I'+index+'"]').val();
        var J = $('input[id="J'+index+'"]').val();
        var K = $('input[id="K'+index+'"]').val();
        var L = $('input[id="L'+index+'"]').val();
        var M = $('input[id="M'+index+'"]').val();
        var N = $('input[id="N'+index+'"]').val();
        var O = $('input[id="O'+index+'"]').val();
        var P = $('input[id="P'+index+'"]').val();
        var Q = $('input[id="Q'+index+'"]').val();
        var R = $('input[id="R'+index+'"]').val();
        var S = $('input[id="S'+index+'"]').val();
        var T = $('select[id="T'+index+'"]').val();
        var U = $('select[id="U'+index+'"]').val();

        item = {};
        item ['B'] = B;
        item ['C'] = C;
        item ['D'] = D;
        item ['E'] = E;
        item ['F'] = F;
        item ['G'] = G;
        item ['H'] = H;
        item ['I'] = I;
        item ['J'] = J;
        item ['K'] = K;
        item ['L'] = L;
        item ['M'] = M;
        item ['N'] = N;
        item ['O'] = O;
        item ['P'] = P;
        item ['Q'] = Q;
        item ['R'] = R;
        item ['S'] = S;
        item ['T'] = T;
        item ['U'] = U;

        jsonObj.push(item);
    };
    // console.log(jsonObj);
    let formatDownload = JSON.stringify(jsonObj);
    let formatDownloadNew = "data='"+formatDownload+"';";
    console.log(formatDownloadNew);
    var save = "text/json;charset=utf-8," + encodeURIComponent(formatDownloadNew);
    $('<a href="data:' + save + '" download="data.json" class="btn" style="text-decoration:none">download JSON</a>').appendTo('#link');    
})

//change 
$(document).on('change','.select1',function(){
    let rowChangeSelect1 = $(this).attr('data-row');
    validateForm(rowChangeSelect1);
    validateFormDisable(rowChangeSelect1);
    validateBChange(rowChangeSelect1);
    $('#B'+rowChangeSelect1).change(function(){
        $(document).on('click','#get-data',function(){
            $('#valF'+rowChangeSelect1).text("space_end")
        })
    })
})

//pengaturan button
$(document).on('click','#button',function(){
    let numberRow = $('#banyak').val();
    addRow(numberRow);

    $('#modaldesc').on('click',function(){
        $('#modalDesc').modal('show');
    })

    $('#modalcharge').on('click',function(){
        $('#modalCharge').modal('show');
    })

    $('#modalbonus').on('click',function(){
        $('#modalBonus').modal('show');
    })

    $('#buttondesc').on('click',function(){
        let desc = document.getElementById('inputdesc').value
        let result = desc.split('\n');
    
        for (let index = 1; index <= numberRow; index++) {
            $('#C'+index).text(result[index-1]);
        }
        $('#modalDesc').modal('hide');
    })

    $('#buttoncharge').on('click',function(){
        let charge = document.getElementById('inputcharge').value
        let result = charge.split('\n');
        
        for (let index = 1; index <= numberRow; index++) {
            $('#O'+index).val(result[index-1]);
        }
        $('#modalCharge').modal('hide');
    })

    $('#buttonbonus').on('click',function(){
        let bonus = document.getElementById('inputbonus').value
        let result = bonus.split('\n');
        
        for (let index = 1; index <= numberRow; index++) {
            $('#P'+index).val(result[index-1]);
        }
        $('#modalBonus').modal('hide');
    })

    $('.modal-show').on('click',function(){
        let dataModal = $(this).attr('data-modal');
        $('#myModal'+dataModal).modal('show')
    })
});


//button load click
$(document).on('click','#load', function(){
    data = dataJson;
    addRow(data.length);
    var noRow = 1;
    $.each(data,function(index,val){
        $.each(column,function(idx,columnName){
            $('#'+columnName+noRow).val(val[columnName]);
        });
        noRow++;
    });

    $('.select1').trigger('change');

    $('#modaldesc').on('click',function(){
        $('#modalDesc').modal('show');
    })

    $('#modalcharge').on('click',function(){
        $('#modalCharge').modal('show');
    })

    $('#modalbonus').on('click',function(){
        $('#modalBonus').modal('show');
    })

    $('#buttondesc').on('click',function(){
        let desc = document.getElementById('inputdesc').value
        let result = desc.split('\n');
    
        for (let index = 1; index <= noRow; index++) {
            $('#C'+index).text(result[index-1]);
        }
        $('#modalDesc').modal('hide');
    })

    $('#buttoncharge').on('click',function(){
        let charge = document.getElementById('inputcharge').value
        let result = charge.split('\n');
        
        for (let index = 1; index <= noRow; index++) {
            $('#O'+index).val(result[index-1]);
        }
        $('#modalCharge').modal('hide');
    })

    $('#buttonbonus').on('click',function(){
        let bonus = document.getElementById('inputbonus').value
        let result = bonus.split('\n');
        
        for (let index = 1; index <= noRow; index++) {
            $('#P'+index).val(result[index-1]);
        }
        $('#modalBonus').modal('hide');
    })

    $('.modal-show').on('click',function(){
        let dataModal = $(this).attr('data-modal');
        $('#myModal'+dataModal).modal('show')
    });

    $(document).on('click','#getUUID',function (){
        // console.log('test')
        var UUID = uuidv4();
        for (let x = 1; x <= noRow; x++) {
            $('#text_uuidA'+x).text(UUID);
            $('#text_uuidB'+x).text(UUID);
            $('#text_uuidC'+x).text(UUID);
            $('#text_uuidD'+x).text(UUID);
            $('#text_uuidE'+x).text(UUID);
        }
    });
});

//penambahan row
$(document).on('click','#ActionAdd',function(){
    console.log('clicked');
    var counter = $('#myTable tr').length;
    addRow(counter++);
});

//penghapusan row
$(document).on('click','#RemoveRow',function(){
    console.log('clicked');
    $(this).closest('tr').remove();
    $('.nomer').text(function(i){
        return i+1
    })
});


//$(document).on('click','#clear-data',function(){
//    $('#bodybawah').empty();
//})
